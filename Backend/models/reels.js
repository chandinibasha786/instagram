const mongoose=require('mongoose')
const {ObjectId}=mongoose.Schema.Types

const reelsSchema=new mongoose.Schema({
    reels:{
    type:String,
    required:true
    },
    postedby:{
        type:ObjectId,
        ref:"User"
    }
})


mongoose.model("Reels",reelsSchema)