const express=require('express')
const app=express()
const mongoose=require('mongoose')
const PORT=4000;
const {MONGOURI}=require('./keys')




mongoose.connect(MONGOURI)

mongoose.connection.on('connected',()=>{
    console.log("connected to mongo")
})
mongoose.connection.on('error',(err)=>{
    console.log("not connected",err)
})

require('./models/user')
require('./models/post')
require('./models/comments')
require('./models/reels')

app.use(express.json())

app.use(require('./routes/auth'))
app.use(require('./routes/post'))
app.use(require('./routes/comments'))
app.use(require('./routes/reels'))


app.listen(PORT,()=>{
    console.log("listening to this port",PORT)
})