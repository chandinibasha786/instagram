const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const Comments = mongoose.model("Comments");
const requireLogin=require('../middleware/requireLogin')

//we have to keep requireLogin here in between /createpost and (req,res) for the purpose of authorized user.
router.get('/allcomments',requireLogin,(req,res)=>{
  Comments.find()
  .populate("postedby","_id name")
  .then(posts=>{
    res.json({posts})
  })
  .catch(err=>{
    console.log(err)
  })
})


router.post('/createcomment',requireLogin,(req,res)=>{
    const {body}=req.body
    if(!body){
      return  res.status(422).json({error:"please add all the fields"})
    }
    // console.log(req.user)
    // res.send("hello")
    req.user.password=undefined
    const comment=new Comments({
        body,
        postedby:req.user
    })
    comment.save()
    .then(result=>{
      res.json({comment:result})
    })
    .catch(err=>{
      console.log(err)
    })
})


router.get('/mycomment',requireLogin,(req,res)=>{
  Comments.find({postedby:req.user._id})
  .populate("postedby","_id name")
  .then(mypost=>{
    res.json({mypost})
  })
  .catch(err=>{
    console.log(err)
  })
})




module.exports=router