const express = require("express");
const { append } = require("express/lib/response");
const router = express.Router();
const mongoose = require("mongoose");
const requireLogin=require('../middleware/requireLogin')



router.post("/createstories",requireLogin,(req,res)=>{
    //user can create stories after he successfully logged in in
})

router.get("/getAllStories",requireLogin,(req,res)=>{
    //user can get stories after he successfully logged in in
})
router.get("/getStoriesById",requireLogin,(req,res)=>{
    //user can get stories by Id after he successfully logged in in
})

router.put("/forgotpassword",requireLogin,(req,res)=>{
    //user can reset the password
})
router.post("/createMessageById",requireLogin,(req,res)=>{
    //user can create Message after he successfully logged in in
})
router.get("/getMessageById",requireLogin,(req,res)=>{
    //user can get stories after he successfully logged in in
})
router.delete("/deleteMessageById",requireLogin,(req,res)=>{
    //user can create delete messages by id after he successfully logged in in
})
router.delete("/deleteReelsById",requireLogin,(req,res)=>{
    //user can create delete Reels by id after he successfully logged in in
})
router.delete("/deleteStoriesById",requireLogin,(req,res)=>{
    //user can create delete stories by id after he successfully logged in in
})
router.delete("/deletePostById",requireLogin,(req,res)=>{
    //user can create delete posts by id after he successfully logged in in
})
router.delete("/deleteAccount",requireLogin,(req,res)=>{
    //user can create delete instagram account by id after he successfully logged in in
})





