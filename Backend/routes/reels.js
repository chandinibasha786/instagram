const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const Reels = mongoose.model("Reels");
const requireLogin=require('../middleware/requireLogin')

router.get('/allreels',requireLogin,(req,res)=>{
    Reels.find()
    .populate("postedby","_id name")
    .then(posts=>{
      res.json({posts})
    })
    .catch(err=>{
      console.log(err)
    })
  })
  

router.post('/createreel',requireLogin,(req,res)=>{
    const {reels}=req.body
    if(!reels){
      return  res.status(422).json({error:"please add all the fields"})
    }
    // console.log(req.user)
    // res.send("hello")
    req.user.password=undefined
    const reel=new Reels({
        reels,
        postedby:req.user
    })
    reel.save()
    .then(result=>{
      res.json({comment:result})
    })
    .catch(err=>{
      console.log(err)
    })
})


router.get('/myreels',requireLogin,(req,res)=>{
    Reels.find({postedby:req.user._id})
    .populate("postedby","_id name")
    .then(myreels=>{
      res.json({myreels})
    })
    .catch(err=>{
      console.log(err)
    })
  })

module.exports=router

