//import logo from './logo.svg';
import {
BrowserRouter,
Routes,
Route,
Switch
} from "react-router-dom";
import './App.css';
import Body from './Components/Home/body';
import Login from './Components/Register/login';
import ForgotPassword from './Components/Register/forgot_pwd';
import Messages from './Components/messages';
import UserProfile from './Components/userprofie';
import Register from "./Components/Register/register";



function App() {
  return (
    <BrowserRouter>
    <Switch>
      <Route exact path="/" component={Login}/>
      <Route  path="/forgotpassword" component={ForgotPassword}/>
      {/* <Route exact path="/register" component={Register}/> */}
      <Route  path="/Home" component={Body}/>
      <Route path="/register"component={Register}/>
      <Route  path="/userprofile" component={UserProfile}/>
      <Route  path="/chat" component={Messages}/>
     





    </Switch>
    </BrowserRouter>
    
    
  );
}

export default App;
