import React,{Component} from 'react';
import { Link } from 'react-router-dom';
import '../../../src/register.css'

class Register extends Component{
    render(){
        return (
<main>
        <div class="page">
            <div class="header">
              <h1 class="logo">Instagram</h1>
              <p>Sign up to see photos and videos from your friends.</p>
            </div>
            <div class="container">

              <form action="">
                <input type="text" placeholder="Mobile Number or Email"/>
                <input type="text" placeholder="Full Name"/>
                
                <input type="text" placeholder="Username"/>
                
                <input type="password" placeholder="Password"/>
                <Link to='/'>
                <button>Sign up</button>
                </Link>
              </form>
              
              <ul>
                <li>By signing up, you agree to our</li>
                <li><a href="">Terms</a></li>
                <li><a href="">Data Policy</a></li>
                <li>and</li>
                <li><a href="">Cookies Policy</a> .</li>
             </ul>
            </div>
        </div>
        <div class="option">
          <Link to='/'>
           <p>Have an account? <a href="./login.html">Log in</a></p>
           </Link>
        </div>
        <div class="otherapps">
          <p>Get the app.</p>
          <button type="button"><i class="fab fa-apple"></i> App Store</button>
          <button type="button"><i class="fab fa-google-play"></i> Google Play</button>
        </div>
        <div class="footer">
          <ul>
            <li><a href="">ABOUT</a></li>
            <li><a href="">HELP</a></li>
            <li><a href="">PRESS</a></li>
            <li><a href="">API</a></li>
            <li><a href="">JOBS</a></li>
            <li><a href="">PRIVACY</a></li>
            <li><a href="">TEMS</a></li>
            <li><a href="">LOCATIONS</a></li>
            <li><a href="">TOP ACCOUNTS</a></li>
            <li><a href="">HASHTAGS</a></li>
            <li><a href="">LANGUAGE</a></li>
          </ul>
          <p>© 2022 INSTAGRAM</p>
        </div>
      </main>
      








        );
    }
}

export default Register