import React,{Component} from 'react';
import { Link } from 'react-router-dom';
import '../../login.css'

class Login extends Component{
    render(){
        return(
            <>
            <div class="login_container">

    <div class="box">
        <img class="heading" src="../IMAGES/logo.PNG"/>
        <form class="login-form">
          <div class="login">
            <div class="field">
                <input id="username" type="name" placeholder="Phone number, username, or email" />
                <label for="username">Phone number, username, or email</label>
            </div>
            <div class="field">
                <input id="password" type="password" placeholder="password" />
                <label for="password">Password</label>
                
            </div>
            <div>
           <Link to='/home'>
            <button id="login-button" title="login">Log In</button>
            </Link> 
            </div>
            </div>
            <div class="separator">
            
                <div class="line"></div>
                <p>OR</p>
                <div class="line"></div>
            </div>
            <div class="other">
                <button class="fb-login-btn" type="button">
                    <i class="fa fa-facebook-official fb-icon"></i>
                    <span class="">Log in with Facebook</span>
                </button>

                <Link to="/forgotpassword">
                <a class="forgot-password" href="#">Forgot password?</a></Link>
            </div>
        </form>
    </div>
    <div class="box">
        <Link to='/register'>
        <p>Don't have an account? <a class="signup" href="#">Sign Up</a></p>
        </Link>
    </div>
</div>
            </>






        );
    }
}
export  default Login ;