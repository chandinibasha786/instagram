import React,{Component} from 'react';
import '../../../../src/App.css'
class Feed extends Component{
    render(){
        return(
       <>
            <div class="post_image">
                    <div class="info">
                        <div class="user1">
                            <div class="profile-pic"><img src="../IMAGES/cover 1.png" alt=""/></div>
                            <p class="username">modern_web_channel</p>
                        </div>
                        <img src="../IMAGES/option.PNG" class="options" alt=""/>
                    </div>
                    <img src="../IMAGES/cover 1.png" class="post-image" alt=""/>
                    <div class="post-content">
                        <div class="reaction-wrapper">
                            <img src="../IMAGES/like.PNG" class="icon" alt=""/>
                            <img src="../IMAGES/comment.PNG" class="icon" alt=""/>
                            <img src="../IMAGES/send.PNG" class="icon" alt=""/>
                            <img src="../IMAGES/save.PNG" class="save icon" alt=""/>
                        </div>
                        <p class="likes">1,012 likes</p>
                        <p class="description"><span>username </span> Lorem ipsum dolor sit amet consectetur,
                            adipisicing elit. Pariatur tenetur veritatis placeat, molestiae impedit aut provident eum
                            quo natus molestias?</p>
                        <p class="post-time">2 minutes ago</p>
                    </div>
                    <div class="comment-wrapper">
                        <img src="../IMAGES/smile.PNG" class="icon" alt=""/>
                        <input type="text" class="comment-box" placeholder="Add a comment"/>
                        <button class="comment-btn">post</button>
                    </div>
            </div>
            <div class="post">
                    <div class="info">
                        <div class="user1">
                            <div class="profile-pic"><img src="../IMAGES/cover 2.png" alt=""/></div>
                            <p class="username">modern_web_channel</p>
                        </div>
                        <img src="../IMAGES/option.PNG" class="options" alt=""/>
                    </div>
                    <img src="../IMAGES/cover 1.png" class="post-image" alt=""/>
                    <div class="post-content">
                        <div class="reaction-wrapper">
                            <img src="../IMAGES/like.PNG" class="icon" alt=""/>
                            <img src="../IMAGES/comment.PNG" class="icon" alt=""/>
                            <img src="../IMAGES/send.PNG" class="icon" alt=""/>
                            <img src="../IMAGES/save.PNG" class="save icon" alt=""/>
                        </div>
                        <p class="likes">1,012 likes</p>
                        <p class="description"><span>username </span> Lorem ipsum dolor sit amet consectetur,
                            adipisicing elit. Pariatur tenetur veritatis placeat, molestiae impedit aut provident eum
                            quo natus molestias?</p>
                        <p class="post-time">2 minutes ago</p>
                    </div>
                    <div class="comment-wrapper">
                        <img src="../IMAGES/smile.PNG" class="icon" alt=""/>
                        <input type="text" class="comment-box" placeholder="Add a comment"/>
                        <button class="comment-btn">post</button>
                    </div>
            </div>
            <div class="post">
                    <div class="info">
                        <div class="user1">
                            <div class="profile-pic"><img src="../IMAGES/cover 3.png" alt=""/></div>
                            <p class="username">modern_web_channel</p>
                        </div>
                        <img src="../IMAGES/option.PNG" class="options" alt=""/>
                    </div>
                    <img src="../IMAGES/cover 1.png" class="post-image" alt=""/>
                    <div class="post-content">
                        <div class="reaction-wrapper">
                            <img src="../IMAGES/like.PNG" class="icon" alt=""/>
                            <img src="../IMAGES/comment.PNG" class="icon" alt=""/>
                            <img src="../IMAGES/send.PNG" class="icon" alt=""/>
                            <img src="../IMAGES/save.PNG" class="save icon" alt=""/>
                        </div>
                        <p class="likes">1,012 likes</p>
                        <p class="description"><span>username </span> Lorem ipsum dolor sit amet consectetur,
                            adipisicing elit. Pariatur tenetur veritatis placeat, molestiae impedit aut provident eum
                            quo natus molestias?</p>
                        <p class="post-time">2 minutes ago</p>
                    </div>
                    <div class="comment-wrapper">
                        <img src="../IMAGES/smile.PNG" class="icon" alt=""/>
                        <input type="text" class="comment-box" placeholder="Add a comment"/>
                        <button class="comment-btn">post</button>
                    </div>
            </div>
            
            <div class="post">
                    <div class="info">
                        <div class="user1">
                            <div class="profile-pic"><img src="../IMAGES/cover 4.png" alt=""/></div>
                            <p class="username">modern_web_channel</p>
                        </div>
                        <img src="../IMAGES/option.PNG" class="options" alt=""/>
                    </div>
                    <img src="../IMAGES/cover 1.png" class="post-image" alt=""/>
                    <div class="post-content">
                        <div class="reaction-wrapper">
                            <img src="../IMAGES/like.PNG" class="icon" alt=""/>
                            <img src="../IMAGES/comment.PNG" class="icon" alt=""/>
                            <img src="../IMAGES/send.PNG" class="icon" alt=""/>
                            <img src="../IMAGES/save.PNG" class="save icon" alt=""/>
                        </div>
                        <p class="likes">1,012 likes</p>
                        <p class="description"><span>username </span> Lorem ipsum dolor sit amet consectetur,
                            adipisicing elit. Pariatur tenetur veritatis placeat, molestiae impedit aut provident eum
                            quo natus molestias?</p>
                        <p class="post-time">2 minutes ago</p>
                    </div>
                    <div class="comment-wrapper">
                        <img src="../IMAGES/smile.PNG" class="icon" alt=""/>
                        <input type="text" class="comment-box" placeholder="Add a comment"/>
                        <button class="comment-btn">post</button>
                    </div>
            </div>
            
            <div class="post">
                    <div class="info">
                        <div class="user1">
                            <div class="profile-pic"><img src="../IMAGES/cover 5.png" alt=""/></div>
                            <p class="username">modern_web_channel</p>
                        </div>
                        <img src="../IMAGES/option.PNG" class="options" alt=""/>
                    </div>
                    <img src="../IMAGES/cover 1.png" class="post-image" alt=""/>
                    <div class="post-content">
                        <div class="reaction-wrapper">
                            <img src="../IMAGES/like.PNG" class="icon" alt=""/>
                            <img src="../IMAGES/comment.PNG" class="icon" alt=""/>
                            <img src="../IMAGES/send.PNG" class="icon" alt=""/>
                            <img src="../IMAGES/save.PNG" class="save icon" alt=""/>
                        </div>
                        <p class="likes">1,012 likes</p>
                        <p class="description"><span>username </span> Lorem ipsum dolor sit amet consectetur,
                            adipisicing elit. Pariatur tenetur veritatis placeat, molestiae impedit aut provident eum
                            quo natus molestias?</p>
                        <p class="post-time">2 minutes ago</p>
                    </div>
                    <div class="comment-wrapper">
                        <img src="../IMAGES/smile.PNG" class="icon" alt=""/>
                        <input type="text" class="comment-box" placeholder="Add a comment"/>
                        <button class="comment-btn">post</button>
                    </div>
            </div>
            <div class="post">
                    <div class="info">
                        <div class="user1">
                            <div class="profile-pic"><img src="../IMAGES/cover 6.png" alt=""/></div>
                            <p class="username">modern_web_channel</p>
                        </div>
                        <img src="../IMAGES/option.PNG" class="options" alt=""/>
                    </div>
                    <img src="../IMAGES/cover 1.png" class="post-image" alt=""/>
                    <div class="post-content">
                        <div class="reaction-wrapper">
                            <img src="../IMAGES/like.PNG" class="icon" alt=""/>
                            <img src="../IMAGES/comment.PNG" class="icon" alt=""/>
                            <img src="../IMAGES/send.PNG" class="icon" alt=""/>
                            <img src="../IMAGES/save.PNG" class="save icon" alt=""/>
                        </div>
                        <p class="likes">1,012 likes</p>
                        <p class="description"><span>username </span> Lorem ipsum dolor sit amet consectetur,
                            adipisicing elit. Pariatur tenetur veritatis placeat, molestiae impedit aut provident eum
                            quo natus molestias?</p>
                        <p class="post-time">2 minutes ago</p>
                    </div>
                    <div class="comment-wrapper">
                        <img src="../IMAGES/smile.PNG" class="icon" alt=""/>
                        <input type="text" class="comment-box" placeholder="Add a comment"/>
                        <button class="comment-btn">post</button>
                    </div>
            </div>
            
            
            
                            
            
        </>
        );
    }
}


export default Feed
