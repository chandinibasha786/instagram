import React,{Component} from 'react';
import '../../../src/App.css'
import {Link}  from 'react-router-dom'

class Header extends Component{
    render(){
        return(
            <nav className="navbar">
          <div className="nav-wrapper">
            <img src="../IMAGES/logo.PNG" className="brand-img" alt="not" />
            <input type="text" className="search-box" placeholder="search" />
            <div className="nav-items">
              <Link to='/home'>
              <img src="../IMAGES/home.PNG" className="icon" alt="" /></Link>
              <Link to='/chat'>
              <img src="../IMAGES/messenger.PNG" className="icon" alt="" /></Link>
              <img src="../IMAGES/add.PNG" className="icon" alt="" />
              <Link to="/userprofile">
               <img src="../IMAGES/profile-pic.png" className="icon user-profile"/>
              </Link>
            </div>
          </div>
        </nav>
        





        );
    }
}

export default Header;
