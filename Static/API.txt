//________________user related APIs____________________

//parent path : /user


/**
 *
 * @param {*} req  request object 
 * @param {*} res  response object
 * @param {*} next  calls the next middleware
 * @desc get all posts which the  user has posted
 * @route GET /user/posts
 * @access private and public 
 */
app.get("/posts",(req,res,next)=>{
    //api implementation here
})



/**
 *
 * @param {*} req  request object 
 * @param {*} res  response object
 * @param {*} next  calls the next middleware
 * @desc get all reels which the  user has posted
 * @route GET /user/reels
 * @access private and public 
 */
 app.get("/reels",(req,res,next)=>{
    //api implementation here
})
/**
 *
 * @param {*} req  request object 
 * @param {*} res  response object
 * @param {*} next  calls the next middleware
 * @desc get all stories which the  user has posted
 * @route GET /user/stories
 * @access private and public 
 */
 app.get("/stories",(req,res,next)=>{
    //api implementation here
})

/**
 *
 * @param {*} req  request object 
 * @param {*} res  response object
 * @param {*} next  calls the next middleware
 * @desc get all followers of  user
 * @route GET /user/followers
 * @access private and public 
 */
 app.get("/followers",(req,res,next)=>{
    //api implementation here
})

/**
 *
 * @param {*} req  request object 
 * @param {*} res  response object
 * @param {*} next  calls the next middleware
 * @desc update user profile
 * @route PUT /user/update
 * @access private 
 */
 app.put("/update",(req,res,next)=>{
    //api implementation here
})


/**
 *
 * @param {*} req  request object 
 * @param {*} res  response object
 * @param {*} next  calls the next middleware
 * @desc user can log in
 * @route POST /user/login
 * @access public
 */
 app.post("/login",(req,res,next)=>{
    //api implementation here
})


/**
 *
 * @param {*} req  request object 
 * @param {*} res  response object
 * @param {*} next  calls the next middleware
 * @desc user can register
 * @route POST /user/register
 * @access public
 */
 app.post("/register",(req,res,next)=>{
    //api implementation here
})


/**
 *
 * @param {*} req  request object 
 * @param {*} res  response object
 * @param {*} next  calls the next middleware
 * @desc user can update profile picture
 * @route PUT /user/profile-img
 * @access private
 */
 app.put("/profile-img",(req,res,next)=>{
    //api implementation here
})


/**
 *
 * @param {*} req  request object 
 * @param {*} res  response object
 * @param {*} next  calls the next middleware
 * @desc user can follow another user
 * @route PUT /user/follow
 * @access public 
 */
 app.put("/follow",(req,res,next)=>{
    //api implementation here
})

/**
 *
 * @param {*} req  request object 
 * @param {*} res  response object
 * @param {*} next  calls the next middleware
 * @desc user request for setting a new password 
 * @route POST /user/forgot
 * @access public 
 */
 app.post("/forgot",(req,res,next)=>{
    //api implementation here
})


/**
 *
 * @param {*} req  request object 
 * @param {*} res  response object
 * @param {*} next  calls the next middleware
 * @desc user request for closing an account
 * @route DELETE /user/delete
 * @access private
 */
 app.delete("/delete",(req,res,next)=>{
    //api implementation here
})


/**
 *
 * @param {*} req  request object 
 * @param {*} res  response object
 * @param {*} next  calls the next middleware
 * @desc user can delete a message by id
 * @route DELETE /user/message
 * @access private
 */
 app.delete("/message/:id",(req,res,next)=>{
    //api implementation here
})

/**
 *
 * @param {*} req  request object 
 * @param {*} res  response object
 * @param {*} next  calls the next middleware
 * @desc user can delete a past post by specifying id
 * @route DELETE /user/posts/:id
 * @access private
 */
 app.delete("/posts/:id",(req,res,next)=>{
    //api implementation here
})

/**
 *
 * @param {*} req  request object 
 * @param {*} res  response object
 * @param {*} next  calls the next middleware
 * @desc user can delete a past reel by specifying id
 * @route DELETE /user/reels/:id
 * @access private
 */
 app.delete("/reels/:id",(req,res,next)=>{
    //api implementation here
})

/**
 *
 * @param {*} req  request object 
 * @param {*} res  response object
 * @param {*} next  calls the next middleware
 * @desc user can delete a past story 
 * @route DELETE /user/story
 * @access private
 */
 app.delete("/story,(req,res,next)=>{
    //api implementation here
})

/**
 *
 * @param {*} req  request object 
 * @param {*} res  response object
 * @param {*} next  calls the next middleware
 * @desc user can create a message
 * @route POST /user/message
 * @access private
 */
 app.post("/message",(req,res,next)=>{
    //api implementation here
})

/**
 *
 * @param {*} req  request object 
 * @param {*} res  response object
 * @param {*} next  calls the next middleware
 * @desc user can create a post
 * @route POST /user/posts
 * @access private
 */
 app.post("/posts",(req,res,next)=>{
    //api implementation here
})


/**
 *
 * @param {*} req  request object 
 * @param {*} res  response object
 * @param {*} next  calls the next middleware
 * @param {*} id    url param specifying id of another user
 * @desc user can get all  messages with another user
 * @route GET /user/message/:id
 * @access private
 */
 app.get("/message",(req,res,next)=>{
    //api implementation here
})



//________________homepage related APIs____________________

//parent path : /homepage


/**
 *
 * @param {*} req  request object 
 * @param {*} res  response object
 * @param {*} next  calls the next middleware
 * @desc get all the posts from the user's following list
 * @route GET /homepage/posts
 * @access private 
 */
 app.get("/posts",(req,res,next)=>{
    //api implementation here
})



/**
 *
 * @param {*} req  request object 
 * @param {*} res  response object
 * @param {*} next  calls the next middleware
 * @desc get all the users in db to be shown in searchbar
 * @route GET /homepage/search
 * @access private 
 */
 app.get("/search",(req,res,next)=>{
    //api implementation here
})


/**
 *
 * @param {*} req  request object 
 * @param {*} res  response object
 * @param {*} next  calls the next middleware
 * @desc get suggestions for users to be shown in homepage left sidebar
 * @route GET /homepage/suggestions
 * @access private 
 */
 app.get("/suggestions",(req,res,next)=>{
    //api implementation here
})


//________________post ,reels and story related APIs____________________



/**
 *
 * @param {*} req  request object 
 * @param {*} res  response object
 * @param {*} next  calls the next middleware
 * @desc get comments for specific post by specified id
 * @route GET /posts/:id/comments
 * @access public 
*/
app.get("/posts/:id/comments",(req,res,next)=>{
    //api implementation here
})


/**
 *
 * @param {*} req  request object 
 * @param {*} res  response object
 * @param {*} next  calls the next middleware
 * @desc get comments for specific reel by specified id
 * @route GET /reels/:id/comments
 * @access public 
*/
app.get("/reels/:id/comments",(req,res,next)=>{
    //api implementation here
})
/**
 *
 * @param {*} req  request object 
 * @param {*} res  response object
 * @param {*} next  calls the next middleware
 * @desc get comments for specific stories by specified id
 * @route GET /stories/:id/comments
 * @access public 
*/
app.get("/stories/:id/comments",(req,res,next)=>{
    //api implementation here
})
